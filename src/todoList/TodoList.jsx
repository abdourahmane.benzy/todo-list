import React from 'react';
import TodoListItem from '../todoListItem/TodoListItem';

class TodoList extends React.Component{

    render(){
        const {todos} = this.props;
        return(
            <div className='form-group'>
                <TodoListItem todos={todos}></TodoListItem>
            </div>
        )  
    }

}
export default TodoList;