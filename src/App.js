import React from 'react'
import AddtodoList from './addTodoList/AddTodoList'
import TodoList from './todoList/TodoList';
class App extends React.Component {

  constructor(){
    super();
    this.state={
      todos: []
    }
  }

  render() {
    return (
      <div className='container'>
        <div className='App'>
          <h3>Todo list</h3>
           <AddtodoList addTodoFn={this.addTodo}/>
           <TodoList  todos={this.state.todos}/>
        </div>
      </div>
    )
  }

  componentDidMount=()=>{
    const todos= localStorage.getItem('todos');
    if (todos) {
        const savedTodo= JSON.parse(todos);
        this.setState({
            todos: savedTodo
        })
    } else {
        console.log('Aucune todo')
    }
    
}

  addTodo= async (todo)=> {
    await this.setState({todos: [...this.state.todos, todo]})
    localStorage.setItem('todos', JSON.stringify(this.state.todos))
  }
}
export default App;
