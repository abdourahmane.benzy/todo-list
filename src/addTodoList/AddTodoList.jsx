import React from 'react';


class AddtodoList extends React.Component{
    constructor(){
        super();
        this.state={
            todo: ''
        }
    }


    render(props){
        return(
            <div className="form-group">
                <form className="form" onSubmit={(e)=> this.onSubmitTodo(e)}>
                <input type="text" id='input' className='form-control' onChange={(e)=> this.updateInput(e)} />
                {/* <button type="submit"  className='btn btn-primary mt-2 mb-4'>Submit</button> */}
                {this.buttonStatus}
                </form>
            </div>
        )
    }

   buttonStatus=(<button type="submit" disabled  className='btn btn-primary mt-2 mb-4'>Submit</button>)

    
    onSubmitTodo= (e)=>{
        e.preventDefault();
        if(!document.getElementById('input').value) return;
        this.props.addTodoFn(this.state.todo)
        document.getElementById('input').value=''
    }

    updateInput= (e) =>{
        this.setState({
            todo: e.target.value
        })

        if(e.target.value && e.target.value !=null){
           this.buttonStatus= (<button type="submit"  className='btn btn-primary mt-2 mb-4'>Submit</button>)
       
        }
    }
}
export default AddtodoList;