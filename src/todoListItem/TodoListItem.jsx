
import React from 'react';


class TodoListItem extends React.Component {
   
    render(){
        const {todos} = this.props;
        
        return(
            <>
                {
                    todos.map((_todos,index) =>{
                        return(
                             
                            <ul key={index} onClick={(e)=>this.conpleteTodo(e)} className='text-primary h4'>
                                <li>{_todos}</li>
                            </ul>   
                        )
                    })
                }
            </>
      
        )
    }

    conpleteTodo=(e)=>{

        if (!e.target.classList.value) {
            e.target.classList.add('text-decoration-line-through','text-success') 
        } else {
         e.target.classList.remove('text-decoration-line-through', 'text-success') 
        }
        
    }
}

export default TodoListItem;